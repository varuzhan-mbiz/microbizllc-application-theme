---
title: Customization
description: "Refer to the list of the Kendo UI Default theme variables available for customization."
slug: variables_kendothemedefault_kendouiforangular
position: 9
---

# Customization

The following list describes the theme variables available for adjustment in the Kendo UI Default theme for Angular.

## Variables

### Common

| Name | Default value | Description |
|---|---|---|
| $font-size | 14px | Base font size across all components. |
| $font-family | inherit | Font family across all components. |
| $font-family-monospace | Consolas, "Ubuntu Mono", "Lucida Console", "Courier New", monospace | Font family for monospaced text. Used for styling the code. |
| $line-height | (20 / 14) | Line height used along with $font-size. |
| $border-radius | 2px | Border radius for all components. |
| $accent | #ff6358 | The color that focuses the user attention.Used for primary buttons and for elements of primary importance across the theme. |
| $accent-contrast | #ffffff | The color used along with the accent color denoted by $accent.Used to provide contrast between the background and foreground colors. |
| $base-text | #656565 | The text color of the components' chrome area. |
| $base-bg | #f6f6f6 | The background of the components' chrome area. |
| $base-border | rgba( black, .08 ) | The border color of the components' chrome area. |
| $base-gradient | $base-bg, darken( $base-bg, 2% ) | The gradient background of the components' chrome area. |
| $hovered-text | #656565 | The text color of hovered items. |
| $hovered-bg | #ededed | The background of hovered items. |
| $hovered-border | rgba( black, .15 ) | The border color of hovered items. |
| $hovered-gradient | $hovered-bg, darken( $hovered-bg, 2% ) | The gradient background of hovered items. |
| $selected-text | $accent-contrast | The text color of selected items. |
| $selected-bg | $accent | The background of selected items. |
| $selected-border | rgba( black, .1 ) | The border color of selected items. |
| $selected-gradient | none | The gradient background of selected items. |
| $error | #f5503e | The color for error messages and states. |
| $warning | #fdce3e | The color for warning messages and states. |
| $success | #5ec232 | The color for success messages and states. |
| $info | #3e80ed | The color for informational messages and states. |
| $font-size | 14px | Base font size across all components. |
| $font-family | inherit | Font family across all components. |
| $font-family-monospace | Consolas, "Ubuntu Mono", "Lucida Console", "Courier New", monospace | Font family for monospaced text. Used for styling the code. |
| $line-height | (20 / 14) | Line height used along with $font-size. |
| $border-radius | 2px | Border radius for all components. |
| $accent | #ff6358 | The color that focuses the user attention.Used for primary buttons and for elements of primary importance across the theme. |
| $accent-contrast | #ffffff | The color used along with the accent color denoted by $accent.Used to provide contrast between the background and foreground colors. |
| $base-text | #656565 | The text color of the components' chrome area. |
| $base-bg | #f6f6f6 | The background of the components' chrome area. |
| $base-border | rgba( black, .08 ) | The border color of the components' chrome area. |
| $base-gradient | $base-bg, darken( $base-bg, 2% ) | The gradient background of the components' chrome area. |
| $hovered-text | #656565 | The text color of hovered items. |
| $hovered-bg | #ededed | The background of hovered items. |
| $hovered-border | rgba( black, .15 ) | The border color of hovered items. |
| $hovered-gradient | $hovered-bg, darken( $hovered-bg, 2% ) | The gradient background of hovered items. |
| $selected-text | $accent-contrast | The text color of selected items. |
| $selected-bg | $accent | The background of selected items. |
| $selected-border | rgba( black, .1 ) | The border color of selected items. |
| $selected-gradient | none | The gradient background of selected items. |
| $error | #f5503e | The color for error messages and states. |
| $warning | #fdce3e | The color for warning messages and states. |
| $success | #5ec232 | The color for success messages and states. |
| $info | #3e80ed | The color for informational messages and states. |

### Buttons
 
| Name | Default value | Description |
|---|---|---|
| $button-text | $base-text | The text color of the buttons. |
| $button-bg | $base-bg | The background of the buttons. |
| $button-border | $base-border | The border color of the buttons. |
| $button-gradient | $base-gradient | The background gradient of the buttons. |
| $button-hovered-text | $hovered-text | The text color of hovered buttons. |
| $button-hovered-bg | $hovered-bg | The background of hovered buttons. |
| $button-hovered-border | $hovered-border | The border color of hovered buttons. |
| $button-hovered-gradient | $hovered-gradient | The background gradient of hovered buttons. |
| $button-pressed-text | $button-text | The text color of pressed buttons. |
| $button-pressed-bg | $button-bg | The background color of pressed buttons. |
| $button-pressed-border | $button-border | The border color of pressed buttons. |
| $button-pressed-gradient | none | The background gradient of pressed buttons. |
| $button-focused-shadow | 0 3px 4px 0 rgba(0, 0, 0, .06) | The shadow of focused buttons. |
| $button-text | $base-text | The text color of the buttons. |
| $button-bg | $base-bg | The background of the buttons. |
| $button-border | $base-border | The border color of the buttons. |
| $button-gradient | $base-gradient | The background gradient of the buttons. |
| $button-hovered-text | $hovered-text | The text color of hovered buttons. |
| $button-hovered-bg | $hovered-bg | The background of hovered buttons. |
| $button-hovered-border | $hovered-border | The border color of hovered buttons. |
| $button-hovered-gradient | $hovered-gradient | The background gradient of hovered buttons. |
| $button-pressed-text | $button-text | The text color of pressed buttons. |
| $button-pressed-bg | $button-bg | The background color of pressed buttons. |
| $button-pressed-border | $button-border | The border color of pressed buttons. |
| $button-pressed-gradient | none | The background gradient of pressed buttons. |
| $button-focused-shadow | 0 3px 4px 0 rgba(0, 0, 0, .06) | The shadow of focused buttons. |

### Charts
 
| Name | Default value | Description |
|---|---|---|
| $series-a | #ff6358 | The color of the first series. |
| $series-b | #ffd246 | The clor of the second series. |
| $series-c | #78d237 | The color of the third series. |
| $series-d | #28b4c8 | The color of the fourth series. |
| $series-e | #2d73f5 | The color of the fifth series. |
| $series-f | #aa46be | The color of the sixth series. |
| $chart-major-lines | rgba(0, 0, 0, .08) | The color of the Chart grid lines (major). |
| $chart-minor-lines | rgba(0, 0, 0, .04) | The color of the Chart grid lines (minor). |
| $series-a | #ff6358 | The color of the first series. |
| $series-b | #ffd246 | The clor of the second series. |
| $series-c | #78d237 | The color of the third series. |
| $series-d | #28b4c8 | The color of the fourth series. |
| $series-e | #2d73f5 | The color of the fifth series. |
| $series-f | #aa46be | The color of the sixth series. |
| $chart-major-lines | rgba(0, 0, 0, .08) | The color of the Chart grid lines (major). |
| $chart-minor-lines | rgba(0, 0, 0, .04) | The color of the Chart grid lines (minor). |

### Toolbar
| Name | Default value | Description |
|---|---|---|
| $toolbar-padding-x | $padding-x | The horizontal padding of the container. |
| $toolbar-padding-y | $padding-x | The vertical padding of the container. |
| $toolbar-padding-x | $padding-x | The horizontal padding of the container. |
| $toolbar-padding-y | $padding-x | The vertical padding of the container. |

## Mixins

### `exports`
Outputs a module once, no matter how many times it is included.


#### Parameters
- name : `String` - The name of the exported module.