# Application Theme

This repository is based on [Kendo UI Default Theme](https://github.com/telerik/kendo-theme-default)

## Customization

[Documentation](docs/customization.md)